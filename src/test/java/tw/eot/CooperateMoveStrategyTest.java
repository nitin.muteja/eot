package tw.eot;

import org.junit.Test;
import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Move;

import static org.junit.Assert.assertEquals;

public class CooperateMoveStrategyTest {




    @Test
    public void should_return_cooperate_always(){
        MoveStrategy moveStrategy = new CooperateMoveStrategy();
        Move move = moveStrategy.move();
        assertEquals(Move.Cooperate , move);
    }
}
