package tw.eot;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;
import tw.eot.contracts.Iscanner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class GameTest {


    private Game game;
    private Iscanner scanner;

    @Before
    public void Initialise()
    {
        scanner = mock(Iscanner.class);
        when(scanner.readInput()).thenReturn("Cooperate","Cooperate");
        game = new Game(scanner, new ConsoleMoveStrategy(scanner), new ConsoleMoveStrategy(scanner));
    }

    @Test
    public void should_return_players_score_and_write_on_console()
    {
        game.displayScore();
        verify(scanner, times(1)).writeOutput(any());
    }

    @Test
    public void should_read_moves_of_both_players_and_execute_game()
    {

        game.play();
        verify(scanner, times(5)).writeOutput("Player1 execute a move");
        verify(scanner, times(10)).readInput();
        verify(scanner, times(5)).writeOutput("Player2 execute a move");
        verify(scanner, times(1)).writeOutput("2, 2");
    }

    @Test
    public void should_read_moves_of_both_playersAsCooperateAndCheat_and_execute_game()
    {
        when(scanner.readInput()).thenReturn("Cooperate","Cheat", "Cooperate", "Cheat", "Cooperate", "Cheat", "Cooperate", "Cheat", "Cooperate", "Cheat");
        game.play();
        verify(scanner, times(5)).writeOutput("Player1 execute a move");
        verify(scanner, times(5)).writeOutput("Player2 execute a move");
        verify(scanner, times(1)).writeOutput("-1, 3");
    }
}
