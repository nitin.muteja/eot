package tw.eot;

import org.junit.Test;
import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Move;

import static org.junit.Assert.assertEquals;

public class CheatMoveStrategyTest {



    @Test
    public void should_return_Cheat_always(){
        MoveStrategy moveStrategy = new CheatMoveStrategy();
        Move move = moveStrategy.move();
        assertEquals(Move.Cheat , move);
    }
}
