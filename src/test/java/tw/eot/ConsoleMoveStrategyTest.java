package tw.eot;

import org.junit.Test;
import tw.eot.contracts.MoveStrategy;
import tw.eot.contracts.Iscanner;
import tw.eot.models.Move;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConsoleMoveStrategyTest {


    Iscanner scanner = mock(Iscanner.class);;


    @Test
    public void should_return_input_Cheat_from_console_() {

        when(scanner.readInput())
                .thenReturn("Cheat");
        MoveStrategy moveStrategy = new ConsoleMoveStrategy(scanner);
        Move move = moveStrategy.move();
        assertEquals(Move.Cheat, move);
    }

    @Test
    public void should_return_input_Cooperate_from_console() {

        when(scanner.readInput())
                .thenReturn("Cooperate");
        MoveStrategy moveStrategy = new ConsoleMoveStrategy(scanner);
        Move move = moveStrategy.move();
        assertEquals(Move.Cooperate, move);
    }
}
