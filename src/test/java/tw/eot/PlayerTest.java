package tw.eot;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Player;

public class PlayerTest {

    @Mock
    private MoveStrategy moveStrategy;

    @Test
    public void updateScore()
    {
        Player player=new Player(1, moveStrategy);
        player.addScore(2);
        Assert.assertEquals(2,player.getScore().intValue());
        player.addScore(1);
        Assert.assertEquals(3,player.getScore().intValue());

    }

}
