package tw.eot;
import org.junit.Test;
import tw.eot.Machine;
import tw.eot.models.Move;
import tw.eot.models.ScoreCard;

import static org.junit.Assert.assertEquals;

public class MachineTest {
    @Test
    public void initialize()
    {
        Machine machine = new Machine();
    }

    @Test
    public void should_return_2_2_for_both_cooperate()
    {
        Machine machine = new Machine();
        ScoreCard scoreCard = machine.calculateScore(Move.Cooperate,Move.Cooperate);
        ScoreCard expectedResult = new ScoreCard(2,2);
        assertEquals(expectedResult,scoreCard);
    }

    @Test
    public void should_return_neg1_3_for_cooperate_cheat()
    {
        Machine machine = new Machine();
        ScoreCard scoreCard = machine.calculateScore(Move.Cooperate,Move.Cheat);
        ScoreCard expectedResult = new ScoreCard(-1,3);
        assertEquals(expectedResult,scoreCard);
    }

    @Test
    public void should_return_3_neg1_for_cheat_cooperate()
    {
        Machine machine = new Machine();
        ScoreCard scoreCard = machine.calculateScore(Move.Cheat,Move.Cooperate);
        ScoreCard expectedResult = new ScoreCard(3,-1);
        assertEquals(expectedResult,scoreCard);
    }

    @Test
    public void should_return_0_0_for_cheat_cheat()
    {
        Machine machine = new Machine();
        ScoreCard scoreCard = machine.calculateScore(Move.Cheat,Move.Cheat);
        ScoreCard expectedResult = new ScoreCard(0,0);
        assertEquals(expectedResult,scoreCard);
    }
}
