package tw.eot;

import tw.eot.models.Move;
import tw.eot.models.ScoreCard;

public class Machine {
    public ScoreCard calculateScore(Move move1, Move move2) {

        if(move1.equals(Move.Cooperate)&&move2.equals(Move.Cooperate))
        {
           return new ScoreCard(2,2);
        }
        else if(move1.equals(Move.Cooperate)&&move2.equals(Move.Cheat))
        {
            return new ScoreCard(-1,3);
        }
        else if(move1.equals(Move.Cheat)&&move2.equals(Move.Cooperate))
        {
            return new ScoreCard(3,-1);
        }

       return new ScoreCard(0,0);
    }


}
