package tw.eot;

import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Move;

import java.util.Observable;
import java.util.Observer;

public class CheatMoveStrategy implements MoveStrategy, Observer {

    @Override
    public Move move() {
        return Move.Cheat;
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
