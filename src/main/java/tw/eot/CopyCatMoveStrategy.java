package tw.eot;

import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Move;

import java.util.Observable;
import java.util.Observer;

public class CopyCatMoveStrategy implements MoveStrategy, Observer {

    Move lastMove;

    @Override
    public Move move() {
        if(lastMove != null) {
        return lastMove;
        } else {
           return Move.Cooperate;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
            lastMove = (Move) arg;
    }
}
