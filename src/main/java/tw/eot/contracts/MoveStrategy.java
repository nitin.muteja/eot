package tw.eot.contracts;

import tw.eot.models.Move;

import java.util.Optional;

public interface MoveStrategy {
    Move move();
}
