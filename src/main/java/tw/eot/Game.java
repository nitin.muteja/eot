package tw.eot;

import tw.eot.contracts.Iscanner;
import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Move;
import tw.eot.models.Player;
import tw.eot.models.ScoreCard;

import java.util.Observable;
import java.util.Observer;

public class Game {

    private Player player1;
    private Player player2;
    private Iscanner scanner;
    private final Machine machine;

    CurrentMove currentMove = new CurrentMove();

    public Game(Iscanner scanner, MoveStrategy strategy1, MoveStrategy strategy2) {
        this.scanner = scanner;
        Observer strategy = new CopyCatMoveStrategy();
        player1 = new Player(1, strategy1);
        player2 = new Player(2, strategy2);
       addObserver(strategy1);
       addObserver(strategy2);

        machine = new Machine();
    }

    private void addObserver(MoveStrategy obj)
    {
        if(obj instanceof Observer) {
            currentMove.addObserver((Observer) obj);
        }

    }


    public void displayScore() {
        String output = player1.getScore().toString() + ", "+ player2.getScore().toString();
        this.scanner.writeOutput(output);
    }

    public void play() {
        for(int i = 0; i<5; i++) {
            scanner.writeOutput("Player1 execute a move");
            Move move1 = executeMove(player1);
            scanner.writeOutput("Player2 execute a move");
            Move move2 = executeMove(player2);
            ScoreCard scoreCard = machine.calculateScore(move1, move2);
            player1.addScore(scoreCard.score1);
            player2.addScore(scoreCard.score2);
            displayScore();
        }
    }

    private Move executeMove(Player currentPlayer)
    {
       Move move= currentPlayer.makeMove();
       currentMove.updateMove(move);
       return move;
    }

    private class CurrentMove extends Observable {
        void updateMove(Move move){
            setChanged();
            notifyObservers(move);
        }
    }
}
