package tw.eot;

import tw.eot.contracts.Iscanner;

public class EotMainApplication {

    public static void main(String ... args){
        Iscanner scanner = new CustomScanner();
        Game game = new Game(scanner, new ConsoleMoveStrategy(scanner), new ConsoleMoveStrategy(scanner));
        game.play();
    }
}
