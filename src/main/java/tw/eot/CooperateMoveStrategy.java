package tw.eot;

import tw.eot.contracts.MoveStrategy;
import tw.eot.models.Move;

public class CooperateMoveStrategy implements MoveStrategy {

    @Override
    public Move move() {
        return Move.Cooperate;
    }
}
