package tw.eot;

import tw.eot.contracts.Iscanner;

public class CustomScanner implements Iscanner {

    @Override
    public void writeOutput(String output) {
         System.out.println(output);
    }

    @Override
    public String readInput() {
        java.util.Scanner sc = new java.util.Scanner(System.in);
        return sc.next();
    }
}
