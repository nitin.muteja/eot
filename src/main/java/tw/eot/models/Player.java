package tw.eot.models;

import tw.eot.contracts.MoveStrategy;

public class Player {

    private MoveStrategy moveStrategy;

    public Player(int id, MoveStrategy moveStrategy)
    {
        this.moveStrategy = moveStrategy;
    }

    private int score;

    public Integer getScore()
    {
        return this.score;
    }

    public void setScore(int score)
    {
        this.score=score;
    }


    public void addScore(int score) {
        this.score+=score;
    }

    public Move makeMove() {
        return moveStrategy.move();
    }
}
