package tw.eot.models;

import java.util.Objects;

public class ScoreCard {

    public int score1;
    public int score2;

    public ScoreCard(int score1, int score2)
    {
        this.score1 = score1;
        this.score2 = score2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreCard scoreCard = (ScoreCard) o;
        return score1 == scoreCard.score1 &&
                score2 == scoreCard.score2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(score1, score2);
    }
}
