package tw.eot;

import tw.eot.contracts.MoveStrategy;
import tw.eot.contracts.Iscanner;
import tw.eot.models.Move;


public class ConsoleMoveStrategy implements MoveStrategy {

    private Iscanner scanner;
    public ConsoleMoveStrategy(Iscanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public Move move() {
        return Move.valueOf(this.scanner.readInput());
    }
}
